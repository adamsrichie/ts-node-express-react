module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    roots: [
        "<rootDir>/src"
    ],
    setupFiles: [
        '<rootDir>/enzyme.config.js'
    ],
    snapshotSerializers: [
        "enzyme-to-json/serializer"
    ]
};