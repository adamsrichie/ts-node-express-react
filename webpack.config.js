const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (env) => {
    const plugins = [];

    if (env && env.analyze) {
        plugins.push(new BundleAnalyzerPlugin());
    }

    return {
        entry: {
            App: "./src/client/index.tsx"
        },

        output: {
            filename: "[name].bundle.js",
            chunkFilename: "[name].bundle.js",
            path: path.resolve('public')
        },

        plugins,

        devtool: "source-map",

        resolve: {
            extensions: [".ts", ".tsx", ".js", ".json"]
        },

        module: {
            rules: [
                { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
                { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
            ]
        },

        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        }
    };
}
