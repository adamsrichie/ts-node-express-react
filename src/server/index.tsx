import compression from "compression";
import express from "express";
import helmet = require("helmet");
import parseArgs from "minimist";
import path from "path";
import React from "react";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import App from "../client/App";
import { getCount, State } from "../client/state";
import { create } from "../client/store";

const args = parseArgs(process.argv.slice(2), { alias: { c: "compression" }, boolean: ["compression"] });
const app = express();

app.use(helmet());

if (args.compression) {
    app.use(compression());
}

app.use("/public", express.static(path.resolve("public")));

const html = (content: string, preloadedState: State) =>
`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="root">${content}</div>
    <script>
        window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, "\\u003c")};
    </script>
    <script src="/public/App.bundle.js"></script>
    <script src="/public/vendors~App.bundle.js"></script>
</body>
</html>`;

app.get("/", async (req, res) => {
    const store = create();
    await store.dispatch<any>(getCount());
    const content = renderToString(
        <Provider store={store}>
            <App />
        </Provider>,
    );
    res.send(html(content, store.getState()));
});

const port = 3000;
// tslint:disable-next-line: no-console
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
