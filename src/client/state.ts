import fetch from "cross-fetch";
import { ActionCreator, AnyAction, Reducer } from "redux";
import { ThunkAction } from "redux-thunk";

const url = "https://m.bestbuy.ca/api/mccp/v2/json/search?pageSize=0";

export interface State {
    count?: number;
    loading?: boolean;
}

export const initialState: State = {};

export const types = {
    getCount: "GET_COUNT",
    getCountSuccess: "GET_COUNT_SUCCESS",
};

export const getCount: ActionCreator<ThunkAction<void, State, {}, AnyAction>> =
        () => async (dispatch) => {
    dispatch({ type: types.getCount });
    const result = await fetch(url);
    const json = await result.json();
    dispatch({ type: types.getCountSuccess, count: json.total });
};

export const reducer: Reducer<State> = (state = initialState, action) => {
    switch (action.type) {
        case types.getCount:
            return { ...state, loading: true };
        case types.getCountSuccess:
            return { count: action.count };
    }

    return state;
};
