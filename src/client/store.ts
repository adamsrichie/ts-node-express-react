import { applyMiddleware, createStore, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import thunk from "redux-thunk";
import { reducer, State } from "./state";

export function create(preloadedState?: State): Store<State> {
    return createStore(
        reducer,
        preloadedState,
        composeWithDevTools(
            applyMiddleware(thunk),
        ),
    );
}
