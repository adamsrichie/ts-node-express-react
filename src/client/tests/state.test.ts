jest.mock("cross-fetch", () => jest.fn(() => Promise.resolve({ json: jest.fn(() => Promise.resolve({ total: 5 })) })));

import { getCount, initialState, reducer, State, types } from "../state";

describe("getCount", () => {
    it("should return action", async () => {
        const dispatch = jest.fn();
        const thunk = getCount();
        await thunk(dispatch, jest.fn(), {});
        expect(dispatch).nthCalledWith(1, { type: types.getCount });
        expect(dispatch).lastCalledWith({ type: types.getCountSuccess, count: 5 });
    });
});

describe("reducer", () => {
    it("should return initialState for no state and unknown action", () => {
        const state = reducer(undefined, { type: "TEST_ACTION" });
        expect(state).toBe(initialState);
    });

    it("should return passed state for unknown action", () => {
        const mockState: State = { loading: true };
        const state = reducer(mockState, { type: "TEST_ACTION" });
        expect(state).toBe(mockState);
    });

    it("should return new state with loading true for getCount action", () => {
        const state = reducer(undefined, { type: types.getCount });
        expect(state).not.toBe(initialState);
        expect(state).toEqual({ loading: true });
    });

    it("should return new state with count for getCountSuccess action", () => {
        const state = reducer(undefined, { type: types.getCountSuccess, count: 10 });
        expect(state).not.toBe(initialState);
        expect(state).toEqual({ count: 10 });
    });
});
