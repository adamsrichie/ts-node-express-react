import { mount, shallow } from "enzyme";
import React from "react";
import { act } from "react-dom/test-utils";
import { App } from "../App";

it("should render", () => {
    const wrapper = shallow(<App count={1} loading={false} getCount={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
});

it("should render loading", () => {
    const wrapper = shallow(<App loading={true} getCount={jest.fn()} />);
    expect(wrapper).toMatchSnapshot();
});

it("should getCount on reload click", () => {
    const getCount = jest.fn();
    const wrapper = shallow(<App count={5} loading={false} getCount={getCount} />);
    wrapper.find("button").simulate("click");
    expect(getCount).toBeCalled();
});

it("should getCount on load when count is undefined", async () => {
    const getCount = jest.fn();
    act(() => { mount(<App count={undefined} loading={false} getCount={getCount} />); });
    expect(getCount).toBeCalled();
});
