import React from "react";
import { connect, MapStateToProps } from "react-redux";
import { getCount, State } from "./state";

interface DispatchProps {
    getCount: () => void;
}

export const App: React.FC<State & DispatchProps> = (props) => {
    React.useEffect(() => {
        if (props.count === undefined) {
            props.getCount();
        }
    }, []);

    const onReloadClick = () => props.getCount();

    return (
        <>
            {
                props.loading ?
                <div>Loading...</div> :
                <div>bestbuy.ca has {props.count} product(s)</div>
            }
            <button onClick={onReloadClick} disabled={props.loading}>Reload</button>
        </>
    );
};

const mapStateToProps: MapStateToProps<State, {}, State> = (state) => state;
const mapDispatchToProps = { getCount };

export default connect(mapStateToProps, mapDispatchToProps)(App);
